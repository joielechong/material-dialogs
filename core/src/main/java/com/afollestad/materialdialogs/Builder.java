package com.afollestad.materialdialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.annotation.ArrayRes;
import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IntRange;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.UiThread;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import com.afollestad.materialdialogs.internal.ThemeSingleton;
import com.afollestad.materialdialogs.util.DialogUtils;
import com.afollestad.materialdialogs.util.TypefaceHelper;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/** The class used to construct a MaterialDialog. */
@SuppressWarnings({ "WeakerAccess", "unused", "SameParameterValue", "ConstantConditions" })
public class Builder {

  protected Context mContext;
  protected CharSequence title;
  protected GravityEnum mTitleGravity = GravityEnum.START;
  protected GravityEnum mContentGravity = GravityEnum.START;
  protected GravityEnum mBtnStackedGravity = GravityEnum.END;
  protected GravityEnum mItemsGravity = GravityEnum.START;
  protected GravityEnum mButtonsGravity = GravityEnum.START;
  protected int mButtonRippleColor = 0;
  protected int mTitleColor = -1;
  protected int mContentColor = -1;
  protected CharSequence mContent;
  protected ArrayList<CharSequence> mItems;
  protected CharSequence positiveText;
  protected CharSequence neutralText;
  protected CharSequence negativeText;
  protected boolean positiveFocus;
  protected boolean neutralFocus;
  protected boolean negativeFocus;
  protected View mViewCustom;
  protected int mWidgetColor;
  protected ColorStateList choiceWidgetColor;
  protected ColorStateList mPositiveColor;
  protected ColorStateList negativeColor;
  protected ColorStateList mNeutralColor;
  protected ColorStateList mLinkColor;
  protected MaterialDialog.SingleButtonCallback onPositiveCallback;
  protected MaterialDialog.SingleButtonCallback onNegativeCallback;
  protected MaterialDialog.SingleButtonCallback onNeutralCallback;
  protected MaterialDialog.SingleButtonCallback onAnyCallback;
  protected MaterialDialog.ListCallback listCallback;
  protected MaterialDialog.ListLongCallback listLongCallback;
  protected MaterialDialog.ListCallbackSingleChoice listCallbackSingleChoice;
  protected MaterialDialog.ListCallbackMultiChoice listCallbackMultiChoice;
  protected boolean alwaysCallMultiChoiceCallback = false;
  protected boolean alwaysCallSingleChoiceCallback = false;
  protected Theme theme = Theme.LIGHT;
  protected boolean cancelable = true;
  protected boolean canceledOnTouchOutside = true;
  protected float mContentLineSpacingMultiplier = 1.2f;
  protected int selectedIndex = -1;
  protected Integer[] selectedIndices = null;
  protected Integer[] disabledIndices = null;
  protected boolean autoDismiss = true;
  protected Typeface mRegularFont;
  protected Typeface mMediumFont;
  protected Drawable icon;
  protected boolean limitIconToDefaultSize;
  protected int maxIconSize = -1;
  protected RecyclerView.Adapter<?> adapter;
  protected RecyclerView.LayoutManager layoutManager;
  protected DialogInterface.OnDismissListener dismissListener;
  protected DialogInterface.OnCancelListener cancelListener;
  protected DialogInterface.OnKeyListener keyListener;
  protected DialogInterface.OnShowListener showListener;
  protected StackingBehavior stackingBehavior;
  protected boolean wrapCustomViewInScroll;
  protected int dividerColor;
  protected int backgroundColor;
  protected int itemColor;
  protected boolean indeterminateProgress;
  protected boolean mShowMinMax;
  protected int progress = -2;
  protected int progressMax = 0;
  protected CharSequence inputPrefill;
  protected CharSequence inputHint;
  protected MaterialDialog.InputCallback inputCallback;
  protected boolean inputAllowEmpty;
  protected int inputType = -1;
  protected boolean alwaysCallInputCallback;
  protected int inputMinLength = -1;
  protected int inputMaxLength = -1;
  protected int inputRangeErrorColor = 0;
  protected int[] itemIds;
  protected CharSequence checkBoxPrompt;
  protected boolean checkBoxPromptInitiallyChecked;
  protected CheckBox.OnCheckedChangeListener checkBoxPromptListener;
  protected InputFilter[] inputFilters;

  protected String progressNumberFormat;
  protected NumberFormat progressPercentFormat;
  protected boolean indeterminateIsHorizontalProgress;

  protected boolean titleColorSet = false;
  protected boolean mContentColorSet = false;
  protected boolean itemColorSet = false;
  protected boolean positiveColorSet = false;
  protected boolean neutralColorSet = false;
  protected boolean negativeColorSet = false;
  protected boolean widgetColorSet = false;
  protected boolean dividerColorSet = false;

  @DrawableRes protected int listSelector;
  @DrawableRes protected int btnSelectorStacked;
  @DrawableRes protected int btnSelectorPositive;
  @DrawableRes protected int btnSelectorNeutral;
  @DrawableRes protected int btnSelectorNegative;

  protected Object mTag;

  protected Builder(Context ctx) {
    mContext = ctx;
    final int materialBlue = DialogUtils.getColor(ctx, R.color.md_material_blue_600);

    // Retrieve default accent colors, which are used on the action buttons and progress bars
    mWidgetColor = DialogUtils.resolveColor(ctx, R.attr.colorAccent, materialBlue);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      mWidgetColor = DialogUtils.resolveColor(ctx, android.R.attr.colorAccent, mWidgetColor);
    }

    mPositiveColor = DialogUtils.getActionTextStateList(ctx, mWidgetColor);
    this.negativeColor = DialogUtils.getActionTextStateList(ctx, mWidgetColor);
    mNeutralColor = DialogUtils.getActionTextStateList(ctx, mWidgetColor);
    mLinkColor = DialogUtils.getActionTextStateList(ctx,
        DialogUtils.resolveColor(ctx, R.attr.md_link_color, mWidgetColor));

    int fallback = 0;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      fallback = DialogUtils.resolveColor(ctx, android.R.attr.colorControlHighlight);
    }
    mButtonRippleColor = DialogUtils.resolveColor(ctx, R.attr.md_btn_ripple_color,
        DialogUtils.resolveColor(ctx, R.attr.colorControlHighlight, fallback));

    this.progressPercentFormat = NumberFormat.getPercentInstance();
    this.progressNumberFormat = "%1d/%2d";

    // Set the default theme based on the Activity theme's primary color darkness (more white or
    // more black)
    int primaryTextColor = DialogUtils.resolveColor(ctx, android.R.attr.textColorPrimary);
    this.theme = DialogUtils.isColorDark(primaryTextColor) ? Theme.LIGHT : Theme.DARK;

    // Load theme values from the ThemeSingleton if needed
    checkSingleton();

    // Retrieve gravity settings from global theme attributes if needed
    mTitleGravity = DialogUtils.resolveGravityEnum(ctx, R.attr.md_title_gravity, mTitleGravity);
    mContentGravity =
        DialogUtils.resolveGravityEnum(ctx, R.attr.md_content_gravity, mContentGravity);
    mBtnStackedGravity =
        DialogUtils.resolveGravityEnum(ctx, R.attr.md_btnstacked_gravity, mBtnStackedGravity);
    mItemsGravity = DialogUtils.resolveGravityEnum(ctx, R.attr.md_items_gravity, mItemsGravity);
    mButtonsGravity =
        DialogUtils.resolveGravityEnum(ctx, R.attr.md_buttons_gravity, mButtonsGravity);

    final String mediumFont = DialogUtils.resolveString(ctx, R.attr.md_medium_font);
    final String regularFont = DialogUtils.resolveString(ctx, R.attr.md_regular_font);
    try {
      typeface(mediumFont, regularFont);
    } catch (Throwable ignored) {
    }

    if (mMediumFont == null) {
      try {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
          mMediumFont = Typeface.create("sans-serif-medium", Typeface.NORMAL);
        } else {
          mMediumFont = Typeface.create("sans-serif", Typeface.BOLD);
        }
      } catch (Throwable ignored) {
        mMediumFont = Typeface.DEFAULT_BOLD;
      }
    }
    if (mRegularFont == null) {
      try {
        mRegularFont = Typeface.create("sans-serif", Typeface.NORMAL);
      } catch (Throwable ignored) {
        mRegularFont = Typeface.SANS_SERIF;
        if (mRegularFont == null) mRegularFont = Typeface.DEFAULT;
      }
    }
  }

  public final Context getContext() {
    return mContext;
  }

  public final int getItemColor() {
    return itemColor;
  }

  public final Typeface getRegularFont() {
    return mRegularFont;
  }

  @SuppressWarnings("ConstantConditions")
  private void checkSingleton() {
    if (ThemeSingleton.get(false) == null) return;

    ThemeSingleton s = ThemeSingleton.get();
    if (s.darkTheme) this.theme = Theme.DARK;
    if (s.titleColor != 0) mTitleColor = s.titleColor;
    if (s.contentColor != 0) mContentColor = s.contentColor;
    if (s.positiveColor != null) mPositiveColor = s.positiveColor;
    if (s.neutralColor != null) mNeutralColor = s.neutralColor;
    if (s.negativeColor != null) this.negativeColor = s.negativeColor;
    if (s.itemColor != 0) this.itemColor = s.itemColor;
    if (s.icon != null) this.icon = s.icon;
    if (s.backgroundColor != 0) this.backgroundColor = s.backgroundColor;
    if (s.dividerColor != 0) this.dividerColor = s.dividerColor;
    if (s.btnSelectorStacked != 0) this.btnSelectorStacked = s.btnSelectorStacked;
    if (s.listSelector != 0) this.listSelector = s.listSelector;
    if (s.btnSelectorPositive != 0) this.btnSelectorPositive = s.btnSelectorPositive;
    if (s.btnSelectorNeutral != 0) this.btnSelectorNeutral = s.btnSelectorNeutral;
    if (s.btnSelectorNegative != 0) this.btnSelectorNegative = s.btnSelectorNegative;
    if (s.widgetColor != 0) mWidgetColor = s.widgetColor;
    if (s.linkColor != null) mLinkColor = s.linkColor;

    mTitleGravity = s.titleGravity;
    mContentGravity = s.contentGravity;
    mBtnStackedGravity = s.btnStackedGravity;
    mItemsGravity = s.itemsGravity;
    mButtonsGravity = s.buttonsGravity;
  }

  public Builder title(@StringRes int titleRes) {
    title(mContext.getText(titleRes));
    return this;
  }

  public Builder title(CharSequence title) {
    this.title = title;
    return this;
  }

  public Builder titleGravity(GravityEnum gravity) {
    mTitleGravity = gravity;
    return this;
  }

  public Builder buttonRippleColor(@ColorInt int color) {
    mButtonRippleColor = color;
    return this;
  }

  public Builder buttonRippleColorRes(@ColorRes int colorRes) {
    return buttonRippleColor(DialogUtils.getColor(mContext, colorRes));
  }

  public Builder buttonRippleColorAttr(@AttrRes int colorAttr) {
    return buttonRippleColor(DialogUtils.resolveColor(mContext, colorAttr));
  }

  public Builder titleColor(@ColorInt int color) {
    mTitleColor = color;
    this.titleColorSet = true;
    return this;
  }

  public Builder titleColorRes(@ColorRes int colorRes) {
    return titleColor(DialogUtils.getColor(this.mContext, colorRes));
  }

  public Builder titleColorAttr(@AttrRes int colorAttr) {
    return titleColor(DialogUtils.resolveColor(this.mContext, colorAttr));
  }

  /**
   * Sets the fonts used in the dialog. It's recommended that you use {@link #typeface(String,
   * String)} instead, to avoid duplicate Typeface allocations and high memory usage.
   *
   * @param medium The font used on titles and action buttons. Null uses device default.
   * @param regular The font used everywhere else, like on the mContent and list mItems. Null uses
   * device default.
   * @return The Builder instance so you can chain calls to it.
   */
  public Builder typeface(@Nullable Typeface medium, @Nullable Typeface regular) {
    mMediumFont = medium;
    mRegularFont = regular;
    return this;
  }

  /**
   * Sets the fonts used in the dialog, by file names. This also uses TypefaceHelper in order to
   * avoid any un-needed allocations (it recycles typefaces for you).
   *
   * @param medium The name of font in assets/fonts used on titles and action buttons (null uses
   * device default). E.g. [your-project]/app/main/assets/fonts/[medium]
   * @param regular The name of font in assets/fonts used everywhere else, like mContent and list
   * mItems (null uses device default). E.g. [your-project]/app/main/assets/fonts/[regular]
   * @return The Builder instance so you can chain calls to it.
   */
  public Builder typeface(@Nullable String medium, @Nullable String regular) {
    if (medium != null && !medium.trim().isEmpty()) {
      mMediumFont = TypefaceHelper.get(this.mContext, medium);
      if (mMediumFont == null) {
        throw new IllegalArgumentException("No font asset found for \"" + medium + "\"");
      }
    }
    if (regular != null && !regular.trim().isEmpty()) {
      mRegularFont = TypefaceHelper.get(this.mContext, regular);
      if (mRegularFont == null) {
        throw new IllegalArgumentException("No font asset found for \"" + regular + "\"");
      }
    }
    return this;
  }

  public Builder icon(Drawable icon) {
    this.icon = icon;
    return this;
  }

  public Builder iconRes(@DrawableRes int icon) {
    this.icon = ResourcesCompat.getDrawable(mContext.getResources(), icon, null);
    return this;
  }

  public Builder iconAttr(@AttrRes int iconAttr) {
    this.icon = DialogUtils.resolveDrawable(mContext, iconAttr);
    return this;
  }

  public Builder content(@StringRes int contentRes) {
    return content(contentRes, false);
  }

  public Builder content(@StringRes int contentRes, boolean html) {
    CharSequence text = this.mContext.getText(contentRes);
    if (html) text = Html.fromHtml(text.toString().replace("\n", "<br/>"));
    return content(text);
  }

  public Builder content(CharSequence content) {
    if (mViewCustom != null) {
      throw new IllegalStateException("You cannot set content() when you're using a custom view.");
    }

    mContent = content;
    return this;
  }

  public Builder content(@StringRes int contentRes, Object... formatArgs) {
    String str = String.format(mContext.getString(contentRes), formatArgs)
        .replace("\n", "<br/>");
    //noinspection deprecation
    return content(Html.fromHtml(str));
  }

  public Builder contentColor(@ColorInt int color) {
    mContentColor = color;
    mContentColorSet = true;
    return this;
  }

  public Builder contentColorRes(@ColorRes int colorRes) {
    contentColor(DialogUtils.getColor(mContext, colorRes));
    return this;
  }

  public Builder contentColorAttr(@AttrRes int colorAttr) {
    contentColor(DialogUtils.resolveColor(mContext, colorAttr));
    return this;
  }

  public Builder contentGravity(GravityEnum gravity) {
    mContentGravity = gravity;
    return this;
  }

  public Builder contentLineSpacing(float multiplier) {
    mContentLineSpacingMultiplier = multiplier;
    return this;
  }

  public Builder items(Collection collection) {
    if (collection.size() > 0) {
      final CharSequence[] array = new CharSequence[collection.size()];
      int i = 0;
      for (Object obj : collection) {
        array[i] = obj.toString();
        i++;
      }
      items(array);
    } else if (collection.size() == 0) {
      mItems = new ArrayList<>();
    }
    return this;
  }

  public Builder items(@ArrayRes int itemsRes) {
    items(mContext.getResources().getTextArray(itemsRes));
    return this;
  }

  public Builder items(CharSequence... items) {
    if (mViewCustom != null) {
      throw new IllegalStateException("You cannot set items() when you're using a custom view.");
    }
    mItems = new ArrayList<>();
    Collections.addAll(mItems, items);
    return this;
  }

  public Builder itemsCallback(MaterialDialog.ListCallback callback) {
    this.listCallback = callback;
    this.listCallbackSingleChoice = null;
    this.listCallbackMultiChoice = null;
    return this;
  }

  public Builder itemsLongCallback(MaterialDialog.ListLongCallback callback) {
    this.listLongCallback = callback;
    this.listCallbackSingleChoice = null;
    this.listCallbackMultiChoice = null;
    return this;
  }

  public Builder itemsColor(@ColorInt int color) {
    this.itemColor = color;
    this.itemColorSet = true;
    return this;
  }

  public Builder itemsColorRes(@ColorRes int colorRes) {
    return itemsColor(DialogUtils.getColor(mContext, colorRes));
  }

  public Builder itemsColorAttr(@AttrRes int colorAttr) {
    return itemsColor(DialogUtils.resolveColor(mContext, colorAttr));
  }

  public Builder itemsGravity(GravityEnum gravity) {
    mItemsGravity = gravity;
    return this;
  }

  public Builder itemsIds(int[] idsArray) {
    this.itemIds = idsArray;
    return this;
  }

  public Builder itemsIds(@ArrayRes int idsArrayRes) {
    return itemsIds(mContext.getResources().getIntArray(idsArrayRes));
  }

  public Builder buttonsGravity(GravityEnum gravity) {
    mButtonsGravity = gravity;
    return this;
  }

  /**
   * Pass anything below 0 (such as -1) for the selected index to leave all options unselected
   * initially. Otherwise pass the index of an item that will be selected initially.
   *
   * @param selectedIndex The checkbox index that will be selected initially.
   * @param callback The callback that will be called when the presses the positive button.
   * @return The Builder instance so you can chain calls to it.
   */
  public Builder itemsCallbackSingleChoice(int selectedIndex, MaterialDialog.ListCallbackSingleChoice callback) {
    this.selectedIndex = selectedIndex;
    this.listCallback = null;
    this.listCallbackSingleChoice = callback;
    this.listCallbackMultiChoice = null;
    return this;
  }

  /**
   * By default, the single choice callback is only called when the user clicks the positive
   * button or if there are no buttons. Call this to force it to always call on item clicks even
   * if the positive button exists.
   *
   * @return The Builder instance so you can chain calls to it.
   */
  public Builder alwaysCallSingleChoiceCallback() {
    this.alwaysCallSingleChoiceCallback = true;
    return this;
  }

  /**
   * Pass null for the selected indices to leave all options unselected initially. Otherwise pass
   * an array of indices that will be selected initially.
   *
   * @param selectedIndices The radio button indices that will be selected initially.
   * @param callback The callback that will be called when the presses the positive button.
   * @return The Builder instance so you can chain calls to it.
   */
  public Builder itemsCallbackMultiChoice(
      @Nullable Integer[] selectedIndices, MaterialDialog.ListCallbackMultiChoice callback) {
    this.selectedIndices = selectedIndices;
    this.listCallback = null;
    this.listCallbackSingleChoice = null;
    this.listCallbackMultiChoice = callback;
    return this;
  }

  /**
   * Sets indices of mItems that are not clickable. If they are checkboxes or radio buttons, they
   * will not be toggleable.
   *
   * @param disabledIndices The item indices that will be disabled from selection.
   * @return The Builder instance so you can chain calls to it.
   */
  public Builder itemsDisabledIndices(@Nullable Integer... disabledIndices) {
    this.disabledIndices = disabledIndices;
    return this;
  }

  /**
   * By default, the multi choice callback is only called when the user clicks the positive button
   * or if there are no buttons. Call this to force it to always call on item clicks even if the
   * positive button exists.
   *
   * @return The Builder instance so you can chain calls to it.
   */
  public Builder alwaysCallMultiChoiceCallback() {
    this.alwaysCallMultiChoiceCallback = true;
    return this;
  }

  public Builder positiveText(@StringRes int positiveRes) {
    if (positiveRes == 0) return this;

    positiveText(this.mContext.getText(positiveRes));
    return this;
  }

  public Builder positiveText(CharSequence message) {
    this.positiveText = message;
    return this;
  }

  public Builder positiveColor(@ColorInt int color) {
    return positiveColor(DialogUtils.getActionTextStateList(mContext, color));
  }

  public Builder positiveColorRes(@ColorRes int colorRes) {
    return positiveColor(DialogUtils.getActionTextColorStateList(mContext, colorRes));
  }

  public Builder positiveColorAttr(@AttrRes int colorAttr) {
    return positiveColor(DialogUtils.resolveActionTextColorStateList(mContext, colorAttr, null));
  }

  public Builder positiveColor(ColorStateList colorStateList) {
    mPositiveColor = colorStateList;
    this.positiveColorSet = true;
    return this;
  }

  public Builder positiveFocus(boolean isFocusedDefault) {
    this.positiveFocus = isFocusedDefault;
    return this;
  }

  public Builder neutralText(@StringRes int neutralRes) {
    if (neutralRes == 0) return this;

    return neutralText(this.mContext.getText(neutralRes));
  }

  public Builder neutralText(CharSequence message) {
    this.neutralText = message;
    return this;
  }

  public Builder negativeColor(@ColorInt int color) {
    return negativeColor(DialogUtils.getActionTextStateList(mContext, color));
  }

  public Builder negativeColorRes(@ColorRes int colorRes) {
    return negativeColor(DialogUtils.getActionTextColorStateList(this.mContext, colorRes));
  }

  public Builder negativeColorAttr(@AttrRes int colorAttr) {
    return negativeColor(DialogUtils.resolveActionTextColorStateList(mContext, colorAttr, null));
  }

  public Builder negativeColor(ColorStateList colorStateList) {
    this.negativeColor = colorStateList;
    this.negativeColorSet = true;
    return this;
  }

  public Builder negativeText(@StringRes int negativeRes) {
    if (negativeRes == 0) return this;

    return negativeText(this.mContext.getText(negativeRes));
  }

  public Builder negativeText(CharSequence message) {
    this.negativeText = message;
    return this;
  }

  public Builder negativeFocus(boolean isFocusedDefault) {
    this.negativeFocus = isFocusedDefault;
    return this;
  }

  public Builder neutralColor(@ColorInt int color) {
    return neutralColor(DialogUtils.getActionTextStateList(mContext, color));
  }

  public Builder neutralColorRes(@ColorRes int colorRes) {
    return neutralColor(DialogUtils.getActionTextColorStateList(mContext, colorRes));
  }

  public Builder neutralColorAttr(@AttrRes int colorAttr) {
    return neutralColor(DialogUtils.resolveActionTextColorStateList(mContext, colorAttr, null));
  }

  public Builder neutralColor(ColorStateList colorStateList) {
    mNeutralColor = colorStateList;
    this.neutralColorSet = true;
    return this;
  }

  public Builder neutralFocus(boolean isFocusedDefault) {
    this.neutralFocus = isFocusedDefault;
    return this;
  }

  public Builder linkColor(@ColorInt int color) {
    return linkColor(DialogUtils.getActionTextStateList(mContext, color));
  }

  public Builder linkColorRes(@ColorRes int colorRes) {
    return linkColor(DialogUtils.getActionTextColorStateList(mContext, colorRes));
  }

  public Builder linkColorAttr(@AttrRes int colorAttr) {
    return linkColor(DialogUtils.resolveActionTextColorStateList(mContext, colorAttr, null));
  }

  public Builder linkColor(ColorStateList colorStateList) {
    mLinkColor = colorStateList;
    return this;
  }

  public Builder listSelector(@DrawableRes int selectorRes) {
    this.listSelector = selectorRes;
    return this;
  }

  public Builder btnSelectorStacked(@DrawableRes int selectorRes) {
    this.btnSelectorStacked = selectorRes;
    return this;
  }

  public Builder btnSelector(@DrawableRes int selectorRes) {
    this.btnSelectorPositive = selectorRes;
    this.btnSelectorNeutral = selectorRes;
    this.btnSelectorNegative = selectorRes;
    return this;
  }

  public Builder btnSelector(@DrawableRes int selectorRes, DialogAction which) {
    switch (which) {
      default:
        this.btnSelectorPositive = selectorRes;
        break;
      case NEUTRAL:
        this.btnSelectorNeutral = selectorRes;
        break;
      case NEGATIVE:
        this.btnSelectorNegative = selectorRes;
        break;
    }
    return this;
  }

  /**
   * Sets the gravity used for the text in stacked action buttons. By default, it's #{@link
   * GravityEnum#END}.
   *
   * @param gravity The gravity to use.
   * @return The Builder instance so calls can be chained.
   */
  public Builder btnStackedGravity(GravityEnum gravity) {
    mBtnStackedGravity = gravity;
    return this;
  }

  public Builder checkBoxPrompt(CharSequence prompt, boolean initiallyChecked,
      @Nullable CheckBox.OnCheckedChangeListener checkListener) {
    this.checkBoxPrompt = prompt;
    this.checkBoxPromptInitiallyChecked = initiallyChecked;
    this.checkBoxPromptListener = checkListener;
    return this;
  }

  public Builder checkBoxPromptRes(@StringRes int prompt, boolean initiallyChecked,
      @Nullable CheckBox.OnCheckedChangeListener checkListener) {
    return checkBoxPrompt(mContext.getResources().getText(prompt), initiallyChecked, checkListener);
  }

  public Builder customView(@LayoutRes int layoutRes, boolean wrapInScrollView) {
    LayoutInflater li = LayoutInflater.from(mContext);
    return customView(li.inflate(layoutRes, null), wrapInScrollView);
  }

  public Builder customView(View view, boolean wrapInScrollView) {
    if (mContent != null) {
      throw new IllegalStateException("You cannot use customView() when you have mContent set.");
    } else if (mItems != null) {
      throw new IllegalStateException("You cannot use customView() when you have mItems set.");
    } else if (this.inputCallback != null) {
      throw new IllegalStateException("You cannot use customView() with an input dialog");
    } else if (this.progress > -2 || this.indeterminateProgress) {
      throw new IllegalStateException("You cannot use customView() with a progress dialog");
    }
    if (view.getParent() != null && view.getParent() instanceof ViewGroup) {
      ((ViewGroup) view.getParent()).removeView(view);
    }
    mViewCustom = view;
    this.wrapCustomViewInScroll = wrapInScrollView;
    return this;
  }

  /**
   * Makes this dialog a progress dialog.
   *
   * @param indeterminate If true, an infinite circular spinner is shown. If false, a horizontal
   * progress bar is shown that is incremented or set via the built MaterialDialog instance.
   * @param max When indeterminate is false, the max value the horizontal progress bar can get to.
   * @return An instance of the Builder so calls can be chained.
   */
  public Builder progress(boolean indeterminate, int max) {
    if (mViewCustom != null) {
      throw new IllegalStateException("You cannot set progress() when you're using a custom view.");
    }
    if (indeterminate) {
      this.indeterminateProgress = true;
      this.progress = -2;
    } else {
      this.indeterminateIsHorizontalProgress = false;
      this.indeterminateProgress = false;
      this.progress = -1;
      this.progressMax = max;
    }
    return this;
  }

  /**
   * Makes this dialog a progress dialog.
   *
   * @param indeterminate If true, an infinite circular spinner is shown. If false, a horizontal
   * progress bar is shown that is incremented or set via the built MaterialDialog instance.
   * @param max When indeterminate is false, the max value the horizontal progress bar can get to.
   * @param showMinMax For determinate dialogs, the min and max will be displayed to the left
   * (start) of the progress bar, e.g. 50/100.
   * @return An instance of the Builder so calls can be chained.
   */
  public Builder progress(boolean indeterminate, int max, boolean showMinMax) {
    mShowMinMax = showMinMax;
    return progress(indeterminate, max);
  }

  /**
   * hange the format of the small text showing current and maximum units of progress. The default
   * is "%1d/%2d".
   */
  public Builder progressNumberFormat(String format) {
    this.progressNumberFormat = format;
    return this;
  }

  /**
   * Change the format of the small text showing the percentage of progress. The default is
   * NumberFormat.getPercentageInstance().
   */
  public Builder progressPercentFormat(NumberFormat format) {
    this.progressPercentFormat = format;
    return this;
  }

  /**
   * By default, indeterminate progress dialogs will use a circular indicator. You can change it
   * to use a horizontal progress indicator.
   */
  public Builder progressIndeterminateStyle(boolean horizontal) {
    this.indeterminateIsHorizontalProgress = horizontal;
    return this;
  }

  public Builder widgetColor(@ColorInt int color) {
    mWidgetColor = color;
    this.widgetColorSet = true;
    return this;
  }

  public Builder widgetColorRes(@ColorRes int colorRes) {
    return widgetColor(DialogUtils.getColor(mContext, colorRes));
  }

  public Builder widgetColorAttr(@AttrRes int colorAttr) {
    return widgetColor(DialogUtils.resolveColor(mContext, colorAttr));
  }

  public Builder choiceWidgetColor(@Nullable ColorStateList colorStateList) {
    this.choiceWidgetColor = colorStateList;
    return this;
  }

  public Builder dividerColor(@ColorInt int color) {
    this.dividerColor = color;
    this.dividerColorSet = true;
    return this;
  }

  public Builder dividerColorRes(@ColorRes int colorRes) {
    return dividerColor(DialogUtils.getColor(mContext, colorRes));
  }

  public Builder dividerColorAttr(@AttrRes int colorAttr) {
    return dividerColor(DialogUtils.resolveColor(mContext, colorAttr));
  }

  public Builder backgroundColor(@ColorInt int color) {
    this.backgroundColor = color;
    return this;
  }

  public Builder backgroundColorRes(@ColorRes int colorRes) {
    return backgroundColor(DialogUtils.getColor(mContext, colorRes));
  }

  public Builder backgroundColorAttr(@AttrRes int colorAttr) {
    return backgroundColor(DialogUtils.resolveColor(mContext, colorAttr));
  }

  public Builder onPositive(MaterialDialog.SingleButtonCallback callback) {
    this.onPositiveCallback = callback;
    return this;
  }

  public Builder onNegative(MaterialDialog.SingleButtonCallback callback) {
    this.onNegativeCallback = callback;
    return this;
  }

  public Builder onNeutral(MaterialDialog.SingleButtonCallback callback) {
    this.onNeutralCallback = callback;
    return this;
  }

  public Builder onAny(MaterialDialog.SingleButtonCallback callback) {
    this.onAnyCallback = callback;
    return this;
  }

  public Builder theme(Theme theme) {
    this.theme = theme;
    return this;
  }

  public Builder cancelable(boolean cancelable) {
    this.cancelable = cancelable;
    this.canceledOnTouchOutside = cancelable;
    return this;
  }

  public Builder canceledOnTouchOutside(boolean canceledOnTouchOutside) {
    this.canceledOnTouchOutside = canceledOnTouchOutside;
    return this;
  }

  /**
   * This defaults to true. If set to false, the dialog will not automatically be dismissed when
   * an action button is pressed, and not automatically dismissed when the user selects a list
   * item.
   *
   * @param dismiss Whether or not to dismiss the dialog automatically.
   * @return The Builder instance so you can chain calls to it.
   */
  public Builder autoDismiss(boolean dismiss) {
    this.autoDismiss = dismiss;
    return this;
  }

  /**
   * Sets a custom {@link RecyclerView.Adapter} for the dialog's list
   *
   * @param adapter The adapter to set to the list.
   * @param layoutManager The layout manager to use in the RecyclerView. Pass null to use the
   * default linear manager.
   * @return This Builder object to allow for chaining of calls to set methods
   */
  public Builder adapter(RecyclerView.Adapter<?> adapter,
      @Nullable RecyclerView.LayoutManager layoutManager) {
    if (this.mViewCustom != null) {
      throw new IllegalStateException("You cannot set adapter() when you're using a custom view.");
    }
    if (layoutManager != null
        && !(layoutManager instanceof LinearLayoutManager)
        && !(layoutManager instanceof GridLayoutManager)) {
      throw new IllegalStateException("You can currently only use LinearLayoutManager"
          + " and GridLayoutManager with this library.");
    }
    this.adapter = adapter;
    this.layoutManager = layoutManager;
    return this;
  }

  /** Limits the display size of a set icon to 48dp. */
  public Builder limitIconToDefaultSize() {
    this.limitIconToDefaultSize = true;
    return this;
  }

  public Builder maxIconSize(int maxIconSize) {
    this.maxIconSize = maxIconSize;
    return this;
  }

  public Builder maxIconSizeRes(@DimenRes int maxIconSizeRes) {
    return maxIconSize((int) mContext.getResources().getDimension(maxIconSizeRes));
  }

  public Builder showListener(DialogInterface.OnShowListener listener) {
    this.showListener = listener;
    return this;
  }

  public Builder dismissListener(DialogInterface.OnDismissListener listener) {
    this.dismissListener = listener;
    return this;
  }

  public Builder cancelListener(DialogInterface.OnCancelListener listener) {
    this.cancelListener = listener;
    return this;
  }

  public Builder keyListener(DialogInterface.OnKeyListener listener) {
    this.keyListener = listener;
    return this;
  }

  /**
   * Sets action button stacking behavior.
   *
   * @param behavior The behavior of the action button stacking logic.
   * @return The Builder instance so you can chain calls to it.
   */
  public Builder stackingBehavior(StackingBehavior behavior) {
    this.stackingBehavior = behavior;
    return this;
  }

  public Builder input(
      @Nullable CharSequence hint,
      @Nullable CharSequence prefill,
      boolean allowEmptyInput,
      MaterialDialog.InputCallback callback) {
    if (mViewCustom != null) {
      throw new IllegalStateException("You cannot set content() when you're using a custom view.");
    }
    this.inputCallback = callback;
    this.inputHint = hint;
    this.inputPrefill = prefill;
    this.inputAllowEmpty = allowEmptyInput;
    return this;
  }

  public Builder input(@Nullable CharSequence hint, @Nullable CharSequence prefill,
      MaterialDialog.InputCallback callback) {
    return input(hint, prefill, true, callback);
  }

  public Builder input(
      @StringRes int hint,
      @StringRes int prefill,
      boolean allowEmptyInput,
      MaterialDialog.InputCallback callback) {
    return input(
        hint == 0 ? null : mContext.getText(hint),
        prefill == 0 ? null : mContext.getText(prefill),
        allowEmptyInput,
        callback);
  }

  public Builder input(@StringRes int hint, @StringRes int prefill,
      MaterialDialog.InputCallback callback) {
    return input(hint, prefill, true, callback);
  }

  public Builder inputType(int type) {
    this.inputType = type;
    return this;
  }

  public Builder inputRange(
      @IntRange(from = 0, to = Integer.MAX_VALUE) int minLength,
      @IntRange(from = -1, to = Integer.MAX_VALUE) int maxLength) {
    return inputRange(minLength, maxLength, 0);
  }

  /** @param errorColor Pass in 0 for the default red error color (as specified in guidelines). */
  public Builder inputRange(
      @IntRange(from = 0, to = Integer.MAX_VALUE) int minLength,
      @IntRange(from = -1, to = Integer.MAX_VALUE) int maxLength,
      @ColorInt int errorColor) {
    if (minLength < 0) {
      throw new IllegalArgumentException("Min length for input dialogs cannot be less than 0.");
    }
    this.inputMinLength = minLength;
    this.inputMaxLength = maxLength;
    if (errorColor == 0) {
      this.inputRangeErrorColor = DialogUtils.getColor(mContext, R.color.md_edittext_error);
    } else {
      this.inputRangeErrorColor = errorColor;
    }
    if (this.inputMinLength > 0) {
      this.inputAllowEmpty = false;
    }
    return this;
  }

  /**
   * Same as #{@link #inputRange(int, int, int)}, but it takes a color resource ID for the error
   * color.
   */
  public Builder inputRangeRes(
      @IntRange(from = 0, to = Integer.MAX_VALUE) int minLength,
      @IntRange(from = -1, to = Integer.MAX_VALUE) int maxLength,
      @ColorRes int errorColor) {
    return inputRange(minLength, maxLength, DialogUtils.getColor(mContext, errorColor));
  }

  public Builder inputFilters(@Nullable InputFilter... filters) {
    this.inputFilters = filters;
    return this;
  }

  public Builder alwaysCallInputCallback() {
    this.alwaysCallInputCallback = true;
    return this;
  }

  public Builder tag(@Nullable Object tag) {
    mTag = tag;
    return this;
  }

  @UiThread
  public MaterialDialog build() {
    return new MaterialDialog(this);
  }

  @UiThread
  public MaterialDialog show() {
    MaterialDialog dialog = build();
    dialog.show();
    return dialog;
  }
}

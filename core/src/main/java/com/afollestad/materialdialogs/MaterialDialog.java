package com.afollestad.materialdialogs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.AttrRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IntRange;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.UiThread;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import com.afollestad.materialdialogs.internal.MDButton;
import com.afollestad.materialdialogs.internal.MDRootLayout;
import com.afollestad.materialdialogs.internal.MDTintHelper;
import com.afollestad.materialdialogs.util.DialogUtils;
import com.afollestad.materialdialogs.util.RippleHelper;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/** @author Aidan Follestad (afollestad) */
@SuppressWarnings({ "WeakerAccess", "SameParameterValue", "unused" })
public class MaterialDialog extends DialogBase
    implements View.OnClickListener, DefaultRvAdapter.InternalListCallback {

  protected final Builder builder;
  private final Handler handler;
  protected ImageView icon;
  protected TextView title;
  protected TextView content;

  EditText input;
  RecyclerView recyclerView;
  View titleFrame;
  FrameLayout customViewFrame;
  ProgressBar progressBar;
  TextView progressLabel;
  TextView progressMinMax;
  TextView inputMinMax;
  CheckBox checkBoxPrompt;
  MDButton positiveButton;
  MDButton neutralButton;
  MDButton negativeButton;
  ListType listType;
  List<Integer> selectedIndicesList;

  public static Builder with(Context context) {
    //return new MaterialDialog()
    return new Builder(context);
  }

  @SuppressLint("InflateParams")
  protected MaterialDialog(Builder builder) {
    super(builder.mContext, DialogInit.getTheme(builder));
    handler = new Handler();
    this.builder = builder;
    final LayoutInflater inflater = LayoutInflater.from(getContext());
    view = (MDRootLayout) inflater.inflate(DialogInit.getInflateLayout(builder), null);
    DialogInit.init(this);

    // Don't keep a Context reference in the Builder after this point
    builder.mContext = null;
  }

  public final Builder getBuilder() {
    return builder;
  }

  public final void setTypeface(TextView target, @Nullable Typeface t) {
    if (t == null) return;

    int flags = target.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG;
    target.setPaintFlags(flags);
    target.setTypeface(t);
  }

  @Nullable
  public Object getTag() {
    return builder.mTag;
  }

  final void checkIfListInitScroll() {
    if (recyclerView == null) return;

    recyclerView
        .getViewTreeObserver()
        .addOnGlobalLayoutListener(
            new ViewTreeObserver.OnGlobalLayoutListener() {
              @SuppressWarnings("ConstantConditions")
              @Override
              public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                  //noinspection deprecation
                  recyclerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                  recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }

                if (listType == ListType.SINGLE || listType == ListType.MULTI) {
                  int selectedIndex;
                  if (listType == ListType.SINGLE) {
                    if (builder.selectedIndex < 0) {
                      return;
                    }
                    selectedIndex = builder.selectedIndex;
                  } else {
                    if (selectedIndicesList == null || selectedIndicesList.size() == 0) {
                      return;
                    }
                    Collections.sort(selectedIndicesList);
                    selectedIndex = selectedIndicesList.get(0);
                  }

                  final int fSelectedIndex = selectedIndex;
                  recyclerView.post(
                      new Runnable() {
                        @Override
                        public void run() {
                          recyclerView.requestFocus();
                          builder.layoutManager.scrollToPosition(fSelectedIndex);
                        }
                      });
                }
              }
            });
  }

  /** Sets the dialog RecyclerView's adapter/layout manager, and it's item click listener. */
  final void invalidateList() {
    if (recyclerView == null) return;

    if ((builder.mItems == null || builder.mItems.size() == 0) && builder.adapter == null) {
      return;
    }
    if (builder.layoutManager == null) {
      builder.layoutManager = new LinearLayoutManager(getContext());
    }
    if (recyclerView.getLayoutManager() == null) {
      recyclerView.setLayoutManager(builder.layoutManager);
    }
    recyclerView.setAdapter(builder.adapter);
    if (listType != null) {
      ((DefaultRvAdapter) builder.adapter).setCallback(this);
    }
  }

  private boolean onItemSelectedListTypeRegular(int position, boolean longPress) {
    // Default adapter, non choice mode
    if (builder.autoDismiss) {
      // If auto dismiss is enabled, dismiss the dialog when a list item is selected
      dismiss();
    }
    if (!longPress && builder.listCallback != null) {
      builder.listCallback.onSelection(this, view, position, builder.mItems.get(position));
    }
    if (longPress && builder.listLongCallback != null) {
      return builder.listLongCallback.onLongSelection(
          this, view, position, builder.mItems.get(position));
    }
    return true;
  }

  @Override
  public boolean onItemSelected(
      MaterialDialog dialog, View view, int position, CharSequence text, boolean longPress) {
    if (!view.isEnabled()) return false;

    if (listType == null || listType == ListType.REGULAR) {
      return onItemSelectedListTypeRegular(position, longPress);
    } else {
      // Default adapter, choice mode
      if (listType == ListType.MULTI) {
        final CheckBox cb = view.findViewById(R.id.md_control);
        if (!cb.isEnabled()) {
          return false;
        }
        final boolean shouldBeChecked = !selectedIndicesList.contains(position);
        if (shouldBeChecked) {
          // Add the selection to the states first so the callback includes it (when
          // alwaysCallMultiChoiceCallback)
          selectedIndicesList.add(position);
          if (builder.alwaysCallMultiChoiceCallback) {
            // If the checkbox wasn't previously selected, and the callback returns true, add it to
            // the states and check it
            if (sendMultiChoiceCallback()) {
              cb.setChecked(true);
            } else {
              // The callback cancelled selection, remove it from the states
              selectedIndicesList.remove(Integer.valueOf(position));
            }
          } else {
            // The callback was not used to check if selection is allowed, just select it
            cb.setChecked(true);
          }
        } else {
          // Remove the selection from the states first so the callback does not include it (when
          // alwaysCallMultiChoiceCallback)
          selectedIndicesList.remove(Integer.valueOf(position));
          if (builder.alwaysCallMultiChoiceCallback) {
            // If the checkbox was previously selected, and the callback returns true, remove it
            // from the states and uncheck it
            if (sendMultiChoiceCallback()) {
              cb.setChecked(false);
            } else {
              // The callback cancelled unselection, re-add it to the states
              selectedIndicesList.add(position);
            }
          } else {
            // The callback was not used to check if the unselection is allowed, just uncheck it
            cb.setChecked(false);
          }
        }
      } else if (listType == ListType.SINGLE) {
        final RadioButton radio = view.findViewById(R.id.md_control);
        if (!radio.isEnabled()) {
          return false;
        }
        boolean allowSelection = true;
        final int oldSelected = builder.selectedIndex;

        if (builder.autoDismiss && builder.positiveText == null) {
          // If auto dismiss is enabled, and no action button is visible to approve the selection,
          // dismiss the dialog
          dismiss();
          // Don't allow the selection to be updated since the dialog is being dismissed anyways
          allowSelection = false;
          // Update selected index and send callback
          builder.selectedIndex = position;
          sendSingleChoiceCallback(view);
        } else if (builder.alwaysCallSingleChoiceCallback) {
          // Temporarily set the new index so the callback uses the right one
          builder.selectedIndex = position;
          // Only allow the radio button to be checked if the callback returns true
          allowSelection = sendSingleChoiceCallback(view);
          // Restore the old selected index, so the state is updated below
          builder.selectedIndex = oldSelected;
        }
        // Update the checked states
        if (allowSelection) {
          builder.selectedIndex = position;
          radio.setChecked(true);
          builder.adapter.notifyItemChanged(oldSelected);
          builder.adapter.notifyItemChanged(position);
        }
      }
    }
    return true;
  }

  final Drawable getListSelector() {
    if (builder.listSelector != 0) {
      return ResourcesCompat.getDrawable(getContext().getResources(), builder.listSelector, null);
    }
    final Drawable d = DialogUtils.resolveDrawable(getContext(), R.attr.md_list_selector);
    if (d == null) {
      return DialogUtils.resolveDrawable(getContext(), R.attr.md_list_selector);
    } else {
      return d;
    }
  }

  public RecyclerView getRecyclerView() {
    return recyclerView;
  }

  public boolean isPromptCheckBoxChecked() {
    return checkBoxPrompt != null && checkBoxPrompt.isChecked();
  }

  public void setPromptCheckBoxChecked(boolean checked) {
    if (checkBoxPrompt != null) checkBoxPrompt.setChecked(checked);
  }

  /* package */ Drawable getButtonSelector(DialogAction which, boolean isStacked) {
    if (isStacked) {
      if (builder.btnSelectorStacked != 0) {
        return ResourcesCompat.getDrawable(
            getContext().getResources(), builder.btnSelectorStacked, null);
      }
      final Drawable d = DialogUtils.resolveDrawable(getContext(), R.attr.md_btn_stacked_selector);
      if (d == null) {
        return DialogUtils.resolveDrawable(getContext(), R.attr.md_btn_stacked_selector);
      } else {
        return d;
      }
    } else {
      switch (which) {
        default: {
          if (builder.btnSelectorPositive != 0) {
            return ResourcesCompat.getDrawable(
                getContext().getResources(), builder.btnSelectorPositive, null);
          }
          Drawable d = DialogUtils.resolveDrawable(getContext(), R.attr.md_btn_positive_selector);
          if (d == null) {
            d = DialogUtils.resolveDrawable(getContext(), R.attr.md_btn_positive_selector);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
              RippleHelper.applyColor(d, builder.mButtonRippleColor);
            }
          }
          return d;
        }
        case NEUTRAL: {
          if (builder.btnSelectorNeutral != 0) {
            return ResourcesCompat.getDrawable(
                getContext().getResources(), builder.btnSelectorNeutral, null);
          }
          Drawable d = DialogUtils.resolveDrawable(getContext(), R.attr.md_btn_neutral_selector);
          if (d == null) {
            d = DialogUtils.resolveDrawable(getContext(), R.attr.md_btn_neutral_selector);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
              RippleHelper.applyColor(d, builder.mButtonRippleColor);
            }
          }
          return d;
        }
        case NEGATIVE: {
          if (builder.btnSelectorNegative != 0) {
            return ResourcesCompat.getDrawable(
                getContext().getResources(), builder.btnSelectorNegative, null);
          }
          Drawable d = DialogUtils.resolveDrawable(getContext(), R.attr.md_btn_negative_selector);
          if (d == null) {
            d = DialogUtils.resolveDrawable(getContext(), R.attr.md_btn_negative_selector);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
              RippleHelper.applyColor(d, builder.mButtonRippleColor);
            }
          }
          return d;
        }
      }
    }
  }

  private boolean sendSingleChoiceCallback(View v) {
    if (builder.listCallbackSingleChoice == null) return false;

    CharSequence text = null;
    if (builder.selectedIndex >= 0 && builder.selectedIndex < builder.mItems.size()) {
      text = builder.mItems.get(builder.selectedIndex);
    }
    return builder.listCallbackSingleChoice.onSelection(this, v, builder.selectedIndex, text);
  }

  private boolean sendMultiChoiceCallback() {
    if (builder.listCallbackMultiChoice == null) return false;

    Collections.sort(selectedIndicesList); // make sure the indices are in order
    List<CharSequence> selectedTitles = new ArrayList<>();
    for (Integer i : selectedIndicesList) {
      if (i < 0 || i > builder.mItems.size() - 1) continue;

      selectedTitles.add(builder.mItems.get(i));
    }
    return builder.listCallbackMultiChoice.onSelection(
        this,
        selectedIndicesList.toArray(new Integer[selectedIndicesList.size()]),
        selectedTitles.toArray(new CharSequence[selectedTitles.size()]));
  }

  @Override
  public final void onClick(View v) {
    DialogAction tag = (DialogAction) v.getTag();
    switch (tag) {
      case POSITIVE:
        if (builder.onPositiveCallback != null) {
          builder.onPositiveCallback.onClick(this, tag);
        }
        if (!builder.alwaysCallSingleChoiceCallback) {
          sendSingleChoiceCallback(v);
        }
        if (!builder.alwaysCallMultiChoiceCallback) {
          sendMultiChoiceCallback();
        }
        if (builder.inputCallback != null && input != null && !builder.alwaysCallInputCallback) {
          builder.inputCallback.onInput(this, input.getText());
        }
        if (builder.autoDismiss) {
          dismiss();
        }
        break;
      case NEGATIVE:
        if (builder.onNegativeCallback != null) {
          builder.onNegativeCallback.onClick(this, tag);
        }
        if (builder.autoDismiss) {
          cancel();
        }
        break;
      case NEUTRAL:
        if (builder.onNeutralCallback != null) {
          builder.onNeutralCallback.onClick(this, tag);
        }
        if (builder.autoDismiss) {
          dismiss();
        }
        break;
    }
    if (builder.onAnyCallback != null) {
      builder.onAnyCallback.onClick(this, tag);
    }
  }

  @Override
  @UiThread
  public void show() {
    try {
      super.show();
    } catch (WindowManager.BadTokenException e) {
      throw new DialogException(
          "Bad window token, you cannot show a dialog "
              + "before an Activity is created or after it's hidden.");
    }
  }

  /**
   * Retrieves the view of an action button, allowing you to modify properties such as whether or
   * not it's enabled. Use {@link #setActionButton(DialogAction, int)} to change text, since the
   * view returned here is not the view that displays text.
   *
   * @param which The action button of which to get the view for.
   * @return The view from the dialog's layout representing this action button.
   */
  public final MDButton getActionButton(DialogAction which) {
    switch (which) {
      default:
        return positiveButton;
      case NEUTRAL:
        return neutralButton;
      case NEGATIVE:
        return negativeButton;
    }
  }

  /** Retrieves the view representing the dialog as a whole. Be careful with this. */
  public final View getView() {
    return view;
  }

  @Nullable
  public final EditText getInputEditText() {
    return input;
  }

  /**
   * Retrieves the TextView that contains the dialog title. If you want to update the title, use
   * #{@link #setTitle(CharSequence)} instead.
   */
  public final TextView getTitleView() {
    return title;
  }

  /** Retrieves the ImageView that contains the dialog icon. */
  public ImageView getIconView() {
    return icon;
  }

  /**
   * Retrieves the TextView that contains the dialog mContent. If you want to update the mContent
   * (message), use #{@link #setContent(CharSequence)} instead.
   */
  @Nullable
  public final TextView getContentView() {
    return content;
  }

  /**
   * Retrieves the custom view that was inflated or set to the MaterialDialog during building.
   *
   * @return The custom view that was passed into the Builder.
   */
  @Nullable
  public final View getCustomView() {
    return builder.mViewCustom;
  }

  /**
   * Updates an action button's title, causing invalidation to check if the action buttons should be
   * stacked. Setting an action button's text to null is a shortcut for hiding it, too.
   *
   * @param which The action button to update.
   * @param title The new title of the action button.
   */
  @UiThread
  public final void setActionButton(final DialogAction which, @Nullable final CharSequence title) {
    switch (which) {
      default:
        builder.positiveText = title;
        positiveButton.setText(title);
        positiveButton.setVisibility(title == null ? View.GONE : View.VISIBLE);
        break;
      case NEUTRAL:
        builder.neutralText = title;
        neutralButton.setText(title);
        neutralButton.setVisibility(title == null ? View.GONE : View.VISIBLE);
        break;
      case NEGATIVE:
        builder.negativeText = title;
        negativeButton.setText(title);
        negativeButton.setVisibility(title == null ? View.GONE : View.VISIBLE);
        break;
    }
  }

  /**
   * Updates an action button's title, causing invalidation to check if the action buttons should be
   * stacked.
   *
   * @param which The action button to update.
   * @param titleRes The string resource of the new title of the action button.
   */
  public final void setActionButton(DialogAction which, @StringRes int titleRes) {
    setActionButton(which, getContext().getText(titleRes));
  }

  /**
   * Gets whether or not the positive, neutral, or negative action button is visible.
   *
   * @return Whether or not 1 or more action buttons is visible.
   */
  public final boolean hasActionButtons() {
    return numberOfActionButtons() > 0;
  }

  /**
   * Gets the number of visible action buttons.
   *
   * @return 0 through 3, depending on how many should be or are visible.
   */
  @SuppressWarnings("WeakerAccess")
  public final int numberOfActionButtons() {
    int number = 0;
    if (positiveButton.getVisibility() == View.VISIBLE) {
      number++;
    }
    if (neutralButton.getVisibility() == View.VISIBLE) {
      number++;
    }
    if (negativeButton.getVisibility() == View.VISIBLE) {
      number++;
    }
    return number;
  }

  @UiThread
  @Override
  public final void setTitle(CharSequence newTitle) {
    title.setText(newTitle);
  }

  @UiThread
  @Override
  public final void setTitle(@StringRes int newTitleRes) {
    setTitle(getContext().getString(newTitleRes));
  }

  @UiThread
  public final void setTitle(@StringRes int newTitleRes, @Nullable Object... formatArgs) {
    setTitle(getContext().getString(newTitleRes, formatArgs));
  }

  @UiThread
  public void setIcon(@DrawableRes final int resId) {
    icon.setImageResource(resId);
    icon.setVisibility(resId != 0 ? View.VISIBLE : View.GONE);
  }

  @UiThread
  public void setIcon(@Nullable final Drawable d) {
    icon.setImageDrawable(d);
    icon.setVisibility(d != null ? View.VISIBLE : View.GONE);
  }

  @UiThread
  public void setIconAttribute(@AttrRes int attrId) {
    Drawable d = DialogUtils.resolveDrawable(getContext(), attrId);
    setIcon(d);
  }

  @UiThread
  public final void setContent(CharSequence newContent) {
    content.setText(newContent);
    content.setVisibility(TextUtils.isEmpty(newContent) ? View.GONE : View.VISIBLE);
  }

  @UiThread
  public final void setContent(@StringRes int newContentRes) {
    setContent(getContext().getString(newContentRes));
  }

  @UiThread
  public final void setContent(@StringRes int newContentRes, @Nullable Object... formatArgs) {
    setContent(getContext().getString(newContentRes, formatArgs));
  }

  @Nullable
  public final ArrayList<CharSequence> getItems() {
    return builder.mItems;
  }

  @UiThread
  public final void setItems(@Nullable CharSequence... items) {
    if (builder.adapter == null) {
      throw new IllegalStateException(
          "This MaterialDialog instance does not "
              + "yet have an adapter set to it. You cannot use setItems().");
    }
    if (items != null) {
      builder.mItems = new ArrayList<>(items.length);
      Collections.addAll(builder.mItems, items);
    } else {
      builder.mItems = null;
    }
    if (!(builder.adapter instanceof DefaultRvAdapter)) {
      throw new IllegalStateException(
          "When using a custom adapter, setItems() "
              + "cannot be used. Set mItems through the adapter instead.");
    }
    notifyItemsChanged();
  }

  @UiThread
  public final void notifyItemInserted(@IntRange(from = 0, to = Integer.MAX_VALUE) int index) {
    builder.adapter.notifyItemInserted(index);
  }

  @UiThread
  public final void notifyItemChanged(@IntRange(from = 0, to = Integer.MAX_VALUE) int index) {
    builder.adapter.notifyItemChanged(index);
  }

  @UiThread
  public final void notifyItemsChanged() {
    builder.adapter.notifyDataSetChanged();
  }

  public final int getCurrentProgress() {
    if (progressBar == null) {
      return -1;
    } else {
      return progressBar.getProgress();
    }
  }

  public ProgressBar getProgressBar() {
    return progressBar;
  }

  public final void incrementProgress(final int by) {
    setProgress(getCurrentProgress() + by);
  }

  public final void setProgress(final int progress) {
    if (builder.progress <= -2) {
      Log.w(
          "MaterialDialog",
          "Calling setProgress(int) on an indeterminate progress dialog has no effect!");
      return;
    }
    progressBar.setProgress(progress);
    final String progressNumberFormat = builder.progressNumberFormat;
    final NumberFormat progressPercentFormat = builder.progressPercentFormat;
    handler.post(
        new Runnable() {
          @Override
          public void run() {
            if (progressLabel != null) {
              progressLabel.setText(
                  progressPercentFormat.format(
                      (float) getCurrentProgress() / (float) getMaxProgress()));
            }
            if (progressMinMax != null) {
              progressMinMax.setText(
                  String.format(progressNumberFormat, getCurrentProgress(), getMaxProgress()));
            }
          }
        });
  }

  public final boolean isIndeterminateProgress() {
    return builder.indeterminateProgress;
  }

  public final int getMaxProgress() {
    if (progressBar == null) {
      return -1;
    } else {
      return progressBar.getMax();
    }
  }

  public final void setMaxProgress(final int max) {
    if (builder.progress <= -2) {
      throw new IllegalStateException("Cannot use setMaxProgress() on this dialog.");
    }
    progressBar.setMax(max);
  }

  /**
   * Change the format of the small text showing the percentage of progress. The default is
   * NumberFormat.getPercentageInstance().
   */
  public final void setProgressPercentFormat(NumberFormat format) {
    builder.progressPercentFormat = format;
    setProgress(getCurrentProgress()); // invalidates display
  }

  /**
   * Change the format of the small text showing current and maximum units of progress. The default
   * is "%1d/%2d".
   */
  public final void setProgressNumberFormat(String format) {
    builder.progressNumberFormat = format;
    setProgress(getCurrentProgress()); // invalidates display
  }

  public final boolean isCancelled() {
    return !isShowing();
  }

  /**
   * Convenience method for getting the currently selected index of a single choice list.
   *
   * @return Currently selected index of a single choice list, or -1 if not showing a single choice
   * list
   */
  public int getSelectedIndex() {
    if (builder.listCallbackSingleChoice != null) {
      return builder.selectedIndex;
    } else {
      return -1;
    }
  }

  /**
   * Convenience method for setting the currently selected index of a single choice list. This only
   * works if you are not using a custom adapter; if you're using a custom adapter, an
   * IllegalStateException is thrown. Note that this does not call the respective single choice
   * callback.
   *
   * @param index The index of the list item to check.
   */
  @UiThread
  public void setSelectedIndex(int index) {
    builder.selectedIndex = index;
    if (builder.adapter != null && builder.adapter instanceof DefaultRvAdapter) {
      builder.adapter.notifyDataSetChanged();
    } else {
      throw new IllegalStateException(
          "You can only use setSelectedIndex() " + "with the default adapter implementation.");
    }
  }

  /**
   * Convenience method for getting the currently selected indices of a multi choice list
   *
   * @return Currently selected index of a multi choice list, or null if not showing a multi choice
   * list
   */
  @Nullable
  public Integer[] getSelectedIndices() {
    if (builder.listCallbackMultiChoice == null) {
      return null;
    } else {
      return selectedIndicesList.toArray(new Integer[selectedIndicesList.size()]);
    }
  }

  /**
   * Convenience method for setting the currently selected indices of a multi choice list. This only
   * works if you are not using a custom adapter; if you're using a custom adapter, an
   * IllegalStateException is thrown. Note that this does not call the respective multi choice
   * callback.
   *
   * @param indices The indices of the list mItems to check.
   */
  @UiThread
  public void setSelectedIndices(Integer[] indices) {
    selectedIndicesList = new ArrayList<>(Arrays.asList(indices));
    if (builder.adapter != null && builder.adapter instanceof DefaultRvAdapter) {
      builder.adapter.notifyDataSetChanged();
    } else {
      throw new IllegalStateException(
          "You can only use setSelectedIndices() " + "with the default adapter implementation.");
    }
  }

  /** Clears all selected checkboxes from multi choice list dialogs. */
  public void clearSelectedIndices() {
    clearSelectedIndices(true);
  }

  /**
   * Clears all selected checkboxes from multi choice list dialogs.
   *
   * @param sendCallback Defaults to true. True will notify the multi-choice callback, if any.
   */
  public void clearSelectedIndices(boolean sendCallback) {
    if (listType == null || listType != ListType.MULTI) {
      throw new IllegalStateException(
          "You can only use clearSelectedIndices() " + "with multi choice list dialogs.");
    }
    if (builder.adapter != null && builder.adapter instanceof DefaultRvAdapter) {
      if (selectedIndicesList != null) {
        selectedIndicesList.clear();
      }
      builder.adapter.notifyDataSetChanged();
      if (sendCallback && builder.listCallbackMultiChoice != null) {
        sendMultiChoiceCallback();
      }
    } else {
      throw new IllegalStateException(
          "You can only use clearSelectedIndices() " + "with the default adapter implementation.");
    }
  }

  /** Selects all checkboxes in multi choice list dialogs. */
  public void selectAllIndices() {
    selectAllIndices(true);
  }

  /**
   * Selects all checkboxes in multi choice list dialogs.
   *
   * @param sendCallback Defaults to true. True will notify the multi-choice callback, if any.
   */
  public void selectAllIndices(boolean sendCallback) {
    if (listType == null || listType != ListType.MULTI) {
      throw new IllegalStateException(
          "You can only use selectAllIndices() with " + "multi choice list dialogs.");
    }
    if (builder.adapter != null && builder.adapter instanceof DefaultRvAdapter) {
      if (selectedIndicesList == null) {
        selectedIndicesList = new ArrayList<>();
      }
      for (int i = 0; i < builder.adapter.getItemCount(); i++) {
        if (!selectedIndicesList.contains(i)) {
          selectedIndicesList.add(i);
        }
      }
      builder.adapter.notifyDataSetChanged();
      if (sendCallback && builder.listCallbackMultiChoice != null) {
        sendMultiChoiceCallback();
      }
    } else {
      throw new IllegalStateException(
          "You can only use selectAllIndices() with the " + "default adapter implementation.");
    }
  }

  @Override
  public final void onShow(DialogInterface dialog) {
    if (input != null) {
      DialogUtils.showKeyboard(this);
      if (input.getText().length() > 0) {
        input.setSelection(input.getText().length());
      }
    }
    super.onShow(dialog);
  }

  void setInternalInputCallback() {
    if (input == null) return;

    input.addTextChangedListener(
        new TextWatcher() {
          @Override
          public void beforeTextChanged(CharSequence s, int start, int count, int after) {
          }

          @Override
          public void onTextChanged(CharSequence s, int start, int before, int count) {
            final int length = s.toString().length();
            boolean emptyDisabled = false;
            if (!builder.inputAllowEmpty) {
              emptyDisabled = length == 0;
              final View positiveAb = getActionButton(DialogAction.POSITIVE);
              positiveAb.setEnabled(!emptyDisabled);
            }
            invalidateInputMinMaxIndicator(length, emptyDisabled);
            if (builder.alwaysCallInputCallback) {
              builder.inputCallback.onInput(MaterialDialog.this, s);
            }
          }

          @Override
          public void afterTextChanged(Editable s) {
          }
        });
  }

  void invalidateInputMinMaxIndicator(int currentLength, boolean emptyDisabled) {
    if (inputMinMax == null) return;

    if (builder.inputMaxLength > 0) {
      inputMinMax.setText(
          String.format(Locale.getDefault(), "%d/%d", currentLength, builder.inputMaxLength));
      inputMinMax.setVisibility(View.VISIBLE);
    } else {
      inputMinMax.setVisibility(View.GONE);
    }
    final boolean isDisabled =
        (emptyDisabled && currentLength == 0)
            || (builder.inputMaxLength > 0 && currentLength > builder.inputMaxLength)
            || currentLength < builder.inputMinLength;
    final int colorText = isDisabled ? builder.inputRangeErrorColor : builder.mContentColor;
    final int colorWidget = isDisabled ? builder.inputRangeErrorColor : builder.mWidgetColor;
    if (builder.inputMaxLength > 0) {
      inputMinMax.setTextColor(colorText);
    }
    MDTintHelper.setTint(input, colorWidget);
    final View positiveAb = getActionButton(DialogAction.POSITIVE);
    positiveAb.setEnabled(!isDisabled);
  }

  @Override
  public void dismiss() {
    if (input != null) DialogUtils.hideKeyboard(this);

    super.dismiss();
  }

  enum ListType {
    REGULAR,
    SINGLE,
    MULTI;

    public static int getLayoutForType(ListType type) {
      switch (type) {
        case REGULAR:
          return R.layout.md_listitem;
        case SINGLE:
          return R.layout.md_listitem_singlechoice;
        case MULTI:
          return R.layout.md_listitem_multichoice;
        default:
          throw new IllegalArgumentException("Not a valid list type");
      }
    }
  }

  /** A callback used for regular list dialogs. */
  public interface ListCallback {
    void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text);
  }

  /** A callback used for regular list dialogs. */
  public interface ListLongCallback {
    boolean onLongSelection(MaterialDialog dialog, View itemView, int position, CharSequence text);
  }

  /** A callback used for multi choice (check box) list dialogs. */
  public interface ListCallbackSingleChoice {

    /**
     * Return true to allow the radio button to be checked, if the alwaysCallSingleChoice() option
     * is used.
     *
     * @param dialog The dialog of which a list item was selected.
     * @param which The index of the item that was selected.
     * @param text The text of the item that was selected.
     * @return True to allow the radio button to be selected.
     */
    boolean onSelection(
        MaterialDialog dialog, View itemView, int which, @Nullable CharSequence text);
  }

  /** A callback used for multi choice (check box) list dialogs. */
  public interface ListCallbackMultiChoice {

    /**
     * Return true to allow the check box to be checked, if the alwaysCallSingleChoice() option is
     * used.
     *
     * @param dialog The dialog of which a list item was selected.
     * @param which The indices of the mItems that were selected.
     * @param text The text of the mItems that were selected.
     * @return True to allow the checkbox to be selected.
     */
    boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text);
  }

  /** An alternate way to define a single callback. */
  public interface SingleButtonCallback {
    void onClick(MaterialDialog dialog, DialogAction which);
  }

  public interface InputCallback {
    void onInput(MaterialDialog dialog, CharSequence input);
  }

  private static class DialogException extends WindowManager.BadTokenException {
    DialogException(@SuppressWarnings("SameParameterValue") String message) {
      super(message);
    }
  }
}
